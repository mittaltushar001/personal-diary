import React, { Fragment, Component } from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import Landing from "./components/Landing";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import Dashboard from "./components/dasboard/Dashboard";
import Compose from "./components/dasboard/Compose";
import Contact from "./components/Contact";
import Upadte from "./components/dasboard/Update";
import FullPost from "./components/dasboard/FullPost";
import Update from "./components/dasboard/Update";
class App extends Component {
  state = {
    isAuthentication: false,
    landing: true,
  };
  authenticated = () => {
    this.setState({
      isAuthentication: true,
      landing: false,
    });
  };
  render() {
    return (
      <Router>
        <Fragment>
          <Navbar authenticated={this.authenticated} />
          <section>
            <Switch>
              <Route
                exact
                path="/"
                render={() => <Landing authenticated={this.state.landing} />}
              />
              <Route exact path="/register" component={Register} />
              <Route
                exact
                path="/login"
                render={() => <Login authenticated={this.authenticated} />}
              />
              <Route exact path="/dashboard" component={Dashboard} />
              <Route exact path="/compose" component={Compose} />
              <Route exact path="/contact-us" component={Contact} />
              <Route exact path="/update-post" component={Upadte} />
              <Route exact path="/dashboard/posts/:id" component={FullPost} />
              <Route
                exact
                path="/dashboard/update-posts/:id"
                component={Update}
              />
            </Switch>
          </section>
        </Fragment>
      </Router>
    );
  }
}
export default App;
