import React, { Component, Fragment } from 'react'
import '../App.css'
import Footer from './Footer'
export default class Landing extends Component {
    componentDidMount(){
        localStorage.removeItem('token')
    }
    render() {
        return <Fragment>
                <div>
                <div className='main-content'>
                    <div className='content-image'>
                        <div className='text'>
                        <h3>Hey!! If you are curious about writing stories and want to ...</h3>
                        <h3>keep secret that stories!!</h3>
                        <h3>Don't bother about pens and papers </h3>
                        <a href='/login' className='text-redirect'>Use our services :>></a>
                        </div>
                    </div>
                    <div className='steps'>
                        <h3>Get Started</h3>
                        <div className='line'></div>
                        <div className="animcontainer">
                        <div className='progress'/>
                        <ul className='steps-group'>
                            <li>Sign Up</li>
                            <li>Sign In</li>
                            <li>Write Stories</li>
                            <li>Submit</li>
                        </ul>
                    </div>
                    </div>
                </div>
                <Footer/>
                </div>
         </Fragment>
    }
}