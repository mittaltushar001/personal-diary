import React, { Component, Fragment } from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
export default class Compose extends Component {
  state = {
    title: "",
    description: "",
    image: null,
    fireRedirect: false,
  };
  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value,
    });
  };
  handleImageChange = (event) => {
    this.setState({
      image: event.target.files[0],
    });
  };
  onSubmit = (event) => {
    event.preventDefault();
    const { title, description, image } = this.state;
    let form_data = new FormData();
    form_data.append("title", title);
    form_data.append("description", description);
    form_data.append("composer", localStorage.getItem("composer"));
    form_data.append("image", image, image.name);
    console.log(form_data);
    const instance = axios.create({
      baseURL: "http://127.0.0.1:8000/api",
      headers: {
        Authorization: "Token " + localStorage.getItem("token"),
        "content-type": "multipart/form-data",
      },
    });
    instance.post("/stories/", form_data).then(
      (response) => {
        console.log(response);
        this.setState({
          fireRedirect: true,
        });
      },
      (error) => {
        console.log(error);
      }
    );
  };
  render() {
    const { title, description, fireRedirect } = this.state;
    return (
      <Fragment>
        <div className="container mt-5">
          <p className="lead">
            <i className="fa fa-book font-weight-bold" aria-hidden="true" />{" "}
            Write Story
          </p>
          <form onSubmit={(event) => this.onSubmit(event)}>
            <div className="form-group">
              <label htmlFor="title">Title</label>
              <input
                type="text"
                className="form-control"
                id="title"
                placeholder="Enter story title here..."
                onChange={this.handleChange}
                name="title"
                value={title}
              />
            </div>
            <div className="form-group">
              <label htmlFor="description">Decription</label>
              <textarea
                type="text-area"
                className="form-control"
                id="description"
                placeholder="Write description here..."
                rows="6"
                name="description"
                onChange={this.handleChange}
                value={description}
              ></textarea>
            </div>
            <div className="form-group">
              <label htmlFor="image">Upload Image</label>
              <input
                type="file"
                id="image"
                accept="image/png, image/jpeg"
                className="form-control"
                onChange={this.handleImageChange}
                required
              />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
          {fireRedirect && <Redirect to={"/dashboard"} />}
        </div>
      </Fragment>
    );
  }
}
