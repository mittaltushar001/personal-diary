import React, { Component, Fragment } from "react";

export default class Search extends Component {
  state = {
    query: "",
    posts: [],
  };
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
  onSubmit = (event) => {
    event.preventDefault();
    this.props.filteredPost(this.state.query);
  };
  render() {
    const { query } = this.state;
    return (
      <Fragment>
        <div className="container mt-5">
          <form onSubmit={(event) => this.onSubmit(event)}>
            <input
              type="text"
              className="form-control"
              name="query"
              value={query}
              placeholder="Enter story title ...."
              onChange={this.handleChange}
            />
          </form>
        </div>
      </Fragment>
    );
  }
}
