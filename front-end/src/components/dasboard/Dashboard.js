import React, { Component, Fragment } from "react";
import Post from "./Post";
import axios from "axios";
import Search from "./Search";
import { Link } from "react-router-dom";
export default class Dashboard extends Component {
  state = {
    posts: [],
    selectedPostId: null,
  };
  componentWillMount() {
    const instance = axios.create({
      baseURL: "http://127.0.0.1:8000/api",
      headers: { Authorization: "Token " + localStorage.getItem("token") },
    });
    instance.get("/stories/").then(
      (response) => {
        console.log(response.data);
        this.setState({
          posts: response.data,
        });
      },
      (error) => {
        console.log(error);
      }
    );
  }
  postSelectedHandler = (id) => {
    this.setState({
      selectedPostId: id,
    });
  };
  searchedPost = (query) => {
    const instance = axios.create({
      baseURL: "http://127.0.0.1:8000/api",
      headers: { Authorization: "Token " + localStorage.getItem("token") },
    });
    instance.get("/search?title=" + query).then(
      (response) => {
        this.setState({
          posts: response.data,
        });
      },
      (error) => {
        console.log(error);
      }
    );
  };
  renderPosts = () => {
    const { posts } = this.state;
    return posts.map((post) => {
      return (
        <Link
          key={post && post.id ? post.id.toString() : "123"}
          to={{ pathname: this.props.match.url + "/posts/" + post.id }}
        >
          <Post
            title={post.title}
            clicked={() => {
              this.postSelectedHandler(post.id);
            }}
          />
        </Link>
      );
    });
  };
  render() {
    return (
      <Fragment>
        <Search filteredPost={this.searchedPost} />
        {this.renderPosts()}
      </Fragment>
    );
  }
}
