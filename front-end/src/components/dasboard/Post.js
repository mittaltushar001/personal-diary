import React, { Component, Fragment } from 'react'

export default class Post extends Component {
    render() {
        return <Fragment>
                 <div className='container mt-4'>
                    <div className='card' onClick={this.props.clicked}>
                        <div className='card-header'>
                            <p className='font-weight-bold'>{this.props.title}</p>
                        </div>
                    </div>
                 </div>
            </Fragment>
    }
}
