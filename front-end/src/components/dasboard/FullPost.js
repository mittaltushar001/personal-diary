import React, { Component } from "react";
import axios from "axios";
import { Link, Redirect } from "react-router-dom";
export default class FullPost extends Component {
  state = {
    fullPost: {},
    postId: this.props.match.params.id,
    fireRedirect: false,
  };
  componentWillMount() {
    let { postId } = this.state;
    const instance = axios.create({
      baseURL: "http://127.0.0.1:8000/api",
      headers: { Authorization: "Token " + localStorage.getItem("token") },
    });
    instance.get("/stories/" + postId + "/").then(
      (response) => {
        console.log(response.data);
        this.setState({
          fullPost: response.data,
        });
      },
      (error) => {
        console.log(error);
      }
    );
  }
  deletePost = () => {
    let { postId } = this.state;
    const instance = axios.create({
      baseURL: "http://127.0.0.1:8000/api",
      headers: { Authorization: "Token " + localStorage.getItem("token") },
    });
    instance.delete("/stories/" + postId + "/").then(
      (response) => {
        console.log(response.data);
        this.setState({
          fireRedirect: true,
        });
      },
      (error) => {
        console.log(error);
      }
    );
  };
  render() {
    const fullPost = this.state.fullPost;
    const { postId, fireRedirect } = this.state;
    console.log(fullPost);
    return (
      <div className="container mt-4">
        <br />
        <div className="p-3">
          <i className="fa fa-book" aria-hidden="true" />
          &nbsp;
          <span className="font-weight-bold">{fullPost["title"]}</span>
        </div>
        <div className="card">
          <div className="card-body">
            <img src={fullPost["image"]} alt="" width="100%" height="400px" />
            <p>{fullPost["description"]}</p>
          </div>
          <div className="card-footer">
            <div className="footer-items">
              <p>
                <span className="font-weight-bold">Posted on : </span>
                {fullPost["compose_date"]}
              </p>
              <div className="button-group">
                <button onClick={this.deletePost}>Delete</button>
                <Link
                  to={{
                    pathname: "/dashboard/update-posts/" + postId,
                  }}
                  className="update"
                >
                  UPDATE
                </Link>
              </div>
            </div>
          </div>
        </div>
        {fireRedirect && <Redirect to={"/dashboard"} />}
      </div>
    );
  }
}
