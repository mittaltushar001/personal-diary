import React from 'react'
const Footer = () => {
    return (
        <div>
            <footer>
                <h5>Copyright &copy; 2020  
                    <a href="https://github.com/gauravcom" target="_blank" rel="noopener noreferrer">
                        &nbsp; Developer.com
                    </a>
                </h5>
                <p>All Rights Reserved</p>
                <small>Developed & Maintained by Yadav Gaurav</small><br/><br/>
            </footer>
        </div>
    )
}
export default Footer