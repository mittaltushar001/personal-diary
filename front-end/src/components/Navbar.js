import React, { Component, Fragment } from 'react';
import {Link} from 'react-router-dom'
import axios from 'axios'
import '../App.css'
class Navbar extends Component{
    state={
        logout_flag:false
    }
    logout=()=>{
        const url='http://127.0.0.1:8000/api'
        const token =localStorage.getItem('token')
        const headers = {
            "Content-Type": "application/json",
            'Accept' : 'application/json',
            "Authorization": `Token ${token}`
        }
        const authAxios=axios.create({
            baseURL:url,
            headers:headers
        })
        authAxios.post('/logout/').then(response=>{
            console.log(response)
            localStorage.removeItem('token')
            localStorage.removeItem('composer')
            this.setState({
                logout_flag:true
            })
            this.props.authenticated();
        },error=>{
            console.log(error)
        })   
    }
    render(){
        const authenticated_user=localStorage.getItem('token')
        return<Fragment>
            {
                authenticated_user?
                <header className='custom-navbar'>
                <nav className='nav-navigtion'>
                    <div></div>
                    <div className='nav-logo'><Link to='/dashboard'>Dashboard</Link></div>
                    <div className='spacer'/>
                    <div className='custom-nav-items'>
                        <ul>
                            <li><Link to='/compose'>COMPOSE</Link></li>
                            <li>
                                <Link onClick={this.logout} to='/'>
                                <i className='fas fa-sign-out-alt' />{' '}
                                 <span className='hide-sm'>LOGOUT</span>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </nav>
                </header>
                :
                <header className='custom-navbar'>
                    <nav className='nav-navigtion'>
                        <div></div>
                        <div className='nav-logo'><Link to='/'>PERSONAL DIARY</Link></div>
                        <div className='spacer'/>
                        <div className='custom-nav-items'>
                            <ul>
                                <li><Link to='/register'>SIGN UP</Link></li>
                                <li><Link to='/login'>SIGN IN</Link></li>
                                <li><Link to='/contact-us'>CONTACT US</Link></li>
                            </ul>
                        </div>
                    </nav>
                </header>
            }
        </Fragment>
    }
}
export default Navbar;