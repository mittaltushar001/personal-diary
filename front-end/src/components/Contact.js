import React, { Component, Fragment } from 'react'
import axios from 'axios'
import {Redirect} from 'react-router-dom'

class Contact extends Component{
    state={
        email:'',
        country:'',
        phno:'',
        msg:'',
        fireRedirect: false
    }
    handleChange=(event)=>{
        this.setState({
            [event.target.name]:event.target.value
          })
    }
    onSubmit=()=>{
        const {email,country,phno,msg}=this.state
        const data={
            email:email,
            country:country,
            phone:phno,
            msg:msg
        }
        axios.post('http://127.0.0.1:8000/api/contact/',data)
        .then(response=>{
            if(response.status===201){
                this.setState({
                    fireRedirect:true
                  })
            }
        },error=>{
            console.log(error.response.status)
        })
    }
    render(){
        const {email,country,phno,msg,fireRedirect}=this.state;
        return <Fragment>
        <div className='container'><br/><br/>
            <p className='lead'>
               <i className='fas fa-user' /> Contact Us
            </p>
            <form onSubmit={(event)=>this.onSubmit(event)}>
                <div className="form-group">
                    <label htmlFor="email">Email</label>
                    <input type="email" className="form-control" id="email" 
                    placeholder='Enter email here...'
                    onChange={this.handleChange} name='email' value={email}/>
                </div>
                <div className='from-group'>
                    <label htmlFor='country'>Country</label>
                    <select id='country' className='form-control' name='country'
                    onChange={this.handleChange} value={country}>
                          <option value=''>Select country</option>
                          <option value='india'>India</option>
                          <option value='us'>Us</option>
                          <option value='england'>England</option>
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="phno">Phone Number</label>
                    <input type="tel" className="form-control" id="phno" 
                    placeholder='Enter phone number here...'
                    name='phno' onChange={this.handleChange} value={phno}/>
              </div>
              <div className="form-group">
                    <label htmlFor="first-name">Meassage</label>
                    <textarea type="text-area" className="form-control" id="msg" 
                    placeholder='Enter meassage here...' rows='5'
                    name='msg' onChange={this.handleChange} value={msg}></textarea>
              </div>
              <button type='submit' className='btn btn-primary'>Submit</button>
            </form>
            {fireRedirect && (
          <Redirect to={'/'}/>
        )}
        </div>
    </Fragment>
}
}
export default Contact;