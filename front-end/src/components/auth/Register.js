import React, { Fragment,Component } from 'react'
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios'
class Register extends Component {
  state={
    first_name:'',
    last_name:'',
    email:'',
    password:'',
    confirm_password:'',
    error:'',
    fireRedirect: false,
    error_flag:false
  }
  handleChange=(event)=>{
    this.setState({
      [event.target.name]:event.target.value
    })  
  }
  onSubmit=(event)=>{
    event.preventDefault();
    const {first_name,last_name,email,password,confirm_password}=this.state
    const data={
       first_name:first_name,
       last_name:last_name,
       email:email,
       password:password,
       confirm_password:confirm_password
    }
    axios.post('http://127.0.0.1:8000/api/register/',data)
    .then(response=>{
      if(response.status===201){
        this.setState({
          fireRedirect:true
        })
      }
    },error=>{
       this.setState({
         error_flag:true,
         error:error.response.data['error']
       })
    })
  }
    render(){
      const { first_name, last_name,email, password, confirm_password,fireRedirect,error_flag,error} = this.state;
     return <Fragment>
      <div className='container'><br/><br/>
      <p className='text-danger text-center'>
        {error_flag?error:''}
      </p>
      <p className='lead'>
        <i className='fas fa-user' /> Create Your Account
      </p>
        <form onSubmit={(event)=>this.onSubmit(event)}>
              <div className="form-group">
                <label htmlFor="first-name">First Name</label>
                <input type="text" className="form-control" id="first-name" 
                placeholder='Enter first name here...' value={first_name} name='first_name'
                onChange={this.handleChange}/>
              </div>
              <div className="form-group">
                <label htmlFor="last-name">Last Name</label>
                <input type="text" className="form-control" id="last-name" 
                placeholder='Enter last name here ..' value={last_name} name='last_name'
                onChange={this.handleChange}/>
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input type="email" className="form-control" id="email" 
                placeholder='Enter email here ..' value={email} name='email'
                onChange={this.handleChange}/>
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input type="password" className="form-control" id="password" 
                placeholder='Enter password ..' value={password} name='password'
                onChange={this.handleChange}/>
              </div>
              <div className="form-group">
                <label htmlFor="confirm_password">Confirm Password</label>
                <input type="password" className="form-control" id="confirm_password"
                placeholder='Please enter confirm password' value={confirm_password} 
                name='confirm_password'  onChange={this.handleChange}/>
              </div>
              <button type='submit' className='btn btn-primary'>Register</button>
        </form>
        <p>
        Already have an account? <Link to='/login'>Sign In</Link>
      </p>
      {fireRedirect && (
          <Redirect to={'/login'}/>
        )}
      </div>
       </Fragment>
    }
    
}
export default Register;