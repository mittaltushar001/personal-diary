import React,{Fragment, Component} from 'react'
import {Redirect } from 'react-router-dom';
import axios from 'axios'
class Login extends Component{
  state={
    email:'',
    password:'',
    fireRedirect:false,
    error:'',
    error_flag:false
  }
  handleChange=(event)=>{
    this.setState({
      [event.target.name]:event.target.value
    })
  }
  onSubmit=(event)=>{
    event.preventDefault();
    const {email,password}=this.state;
    const data={
      email:email,
      password:password
    }
    axios.post('http://127.0.0.1:8000/api/login/',data)
    .then(response=>{
      const token=response.data.token
      const composer=response.data.composer
      console.log(token)
      if(response.status===200){
        localStorage.setItem('token', token)
        localStorage.setItem('composer',composer)
        this.setState({
          fireRedirect:true
        })
        this.props.authenticated();
      }
    },error=>{
      console.log(error.response.status)
      this.setState({
        error_flag:true,
        error:error.response.data['error']
      })
    })
  }
    render(){
      const {email,password,fireRedirect,error_flag,error}=this.state;
      return <Fragment>
       <div className='container'><br/><br/>
       <p className='text-danger text-center'>
        {error_flag?error:''}
      </p>
          <p className='lead'>
            <i className='fas fa-user' /> Sign Into Your Account
          </p>
           <form onSubmit={(event)=>this.onSubmit(event)}>
             <div className="form-group">
                <label htmlFor="email">Email</label>
                <input type="email" className="form-control" id="email" 
                placeholder='Enter email here ..'
                name='email' onChange={this.handleChange} value={email}/>
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input type="password" className="form-control" id="password" 
                placeholder='Enter password ..'
                name='password' onChange={this.handleChange} value={password}/>
              </div>
              <div className="form-group form-check">
                <input type="checkbox" className="form-check-input" id="check" />
                <label className="form-check-label" htmlFor='check'>Remember me</label>
              </div>
              <button type='submit' className='btn btn-primary'>Login</button>
           </form>
           {fireRedirect && (
          <Redirect to={'/dashboard'}/>
        )}
       </div>
    </Fragment>
}
}
export default Login;