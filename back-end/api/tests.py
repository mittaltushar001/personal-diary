import json
from . import models
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from rest_framework import status


class AuthenticationTestCase(APITestCase):
    """Class tests registration and login  API"""

    def setUp(self):
        data = {
            "first_name": "user1",
            "last_name": "user1",
            "email": "user1@gmail.com",
            "password": "12345",
            "confirm_password": "12345"
        }
        data = json.dumps(data)
        response = self.client.post("/api/register/", data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_registration(self):
        """API call with valid data"""
        data = {
            "first_name": "user2",
            "last_name": "user2",
            "email": "user2@gmail.com",
            "password": "12345",
            "confirm_password": "12345"
        }
        data = json.dumps(data)
        response = self.client.post("/api/register/", data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_registration_fail(self):
        """API call with conflict data"""
        data = {
            "first_name": "user1",
            "last_name": "user1",
            "email": "user1@gmail.com",
            "password": "12345",
            "confirm_password": "12345"
        }
        data = json.dumps(data)
        response = self.client.post("/api/register/", data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_409_CONFLICT)

    def test_login(self):
        """API call with valid credential"""
        data = {
            "email": "user1@gmail.com",
            "password": "12345"
        }
        data = json.dumps(data)
        response = self.client.post("/api/login/", data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_login_fail(self):
        """API call with invalid credential"""
        data = {
            "email": "user2@gmail.com",
            "password": "12345"
        }
        data = json.dumps(data)
        response = self.client.post("/api/login/", data=data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ComposeListTestCase(APITestCase):
    """Class tests get and post API's calls.
    GET:get all stories related to a user
    POST:post a story corresponding to user"""

    def setUp(self):
        self.user = models.User.objects.create_user(email="user1@gmail.com", password="12345")
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

    def test_post_story(self):
        """API call to post story"""
        data = {
            "title": "test",
            "description": "test description",
            "composer": "1"
        }
        response = self.client.post("/api/stories/", data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_story_invalid_user_id(self):
        """API call with invalid composer id"""
        data = {
            "title": "test",
            "description": "test description",
            "composer": "2"
        }
        response = self.client.post("/api/stories/", data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_story_without_token(self):
        """API call to post story without authentication"""
        data = {
            "title": "test",
            "description": "test description",
            "composer": "1"
        }
        self.client.force_authenticate(user=None)
        response = self.client.post("/api/stories/", data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_get_stories_list(self):
        """API call to get all stories related to a user"""
        response = self.client.get("/api/stories/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_story_list_un_authenticated(self):
        """API call to get stories without authorization"""
        self.client.force_authenticate(user=None)
        response = self.client.get("/api/stories/")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class ComposeDetailTestCase(APITestCase):
    """Class tests put,delete and update API call"""

    def setUp(self):
        self.user = models.User.objects.create_user(email="user1@gmail.com", password="12345")
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()
        data = {
            "title": "test",
            "description": "test description",
            "composer": "1"
        }
        response = self.client.post("/api/stories/", data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

    def test_get_a_story(self):
        """API call to get a story by a story Id"""
        response = self.client.get("/api/stories/1/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_a_story_un_authenticated(self):
        """API call without authorization"""
        self.client.force_authenticate(user=None)
        response = self.client.get("/api/stories/1/")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_story(self):
        """API call to update the story"""
        data = {
            "title": "test_update",
            "description": "test description updated ",
            "composer": "1"
        }
        response = self.client.put("/api/stories/1/", data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_story_un_authenticated(self):
        """API call without authorization"""
        data = {
            "title": "test_update",
            "description": "test description updated ",
            "composer": "1"
        }
        self.client.force_authenticate(user=None)
        response = self.client.put("/api/stories/1/", data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_story_un_authenticated(self):
        """API call without authorization"""
        self.client.force_authenticate(user=None)
        response = self.client.delete("/api/stories/1/")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete_story(self):
        """API call to delete story """
        response = self.client.delete("/api/stories/1/")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_logout(self):
        """API call to logout the user"""
        response = self.client.post("/api/logout/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

